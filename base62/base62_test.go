package base62

import (
	"testing"
)

func TestSaturate(t *testing.T) {
	saturated := Saturate("google")

	if saturated != 39228476546 {
		t.Error("We got ->", saturated)
	}
}

func TestDehydrate(t *testing.T) {
	dehydrated := Dehydrate(39228476546)

	if dehydrated != "google" {
		t.Error("We got ->", dehydrated)
	}
}

func TestTrueChr(t *testing.T) {
	input, _ := true_chr(1)

	if input != "1" {
		t.Error("Result should be something else, We got", input)
	}

	input2, _ := true_chr(11)

	if input2 != "B" {
		t.Error("Result should be something else, We got", input2)
	}

	input3, _ := true_chr(37)

	if input3 != "b" {
		t.Error("Result should be something else, We got", input3)
	}
}

func TestReversedString(t *testing.T) {
	reversed := reverse("hello")

	if reversed != "olleh" {
		t.Error("Result should be olleh. We got", reversed)
	}
}

func TestTrueOrd(t *testing.T) {
	input, _ := true_ord('2')

	if input != 2 {
		t.Error("Result should be something else, We got", input)
	}

	input2, _ := true_ord('B')

	if input2 != 11 {
		t.Error("Result should be something else, We got", input)
	}

	input3, _ := true_ord('b')

	if input3 != 37 {
		t.Error("Result should be something else, We got", input)
	}
}
