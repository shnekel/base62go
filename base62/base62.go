package base62

import (
	"errors"
	"log"
	"math"
	"unicode"
)

const (
	Base        = 62
	UpperOffset = 55
	LowerOffset = 61
	DigitOffset = 48
)

func Saturate(key string) int {
	var sum float64

	reversed_key := reverse(key)

	for i, ch := range reversed_key {
		val, err := true_ord(ch)
		if err != nil {
			log.Fatal("Invalid value")
		}
		sum += float64(val) * math.Pow(float64(Base), float64(i))
	}
	return int(sum)
}

func Dehydrate(num int) string {
	if num == 0 {
		return "0"
	}

	var str string

	for num > 0 {
		remainder := num % Base
		val, err := true_chr(remainder)

		if err != nil {
			log.Fatal("Invalid integer here")
		}

		str = val + str
		num = num / Base
	}

	return str
}

func true_ord(ch rune) (int, error) {
	if unicode.IsDigit(ch) {
		return int(ch) - DigitOffset, nil
	} else if int(ch) >= int('A') && int(ch) <= int('Z') {
		return int(ch) - UpperOffset, nil
	} else if int(ch) >= int('a') && int(ch) <= int('z') {
		return int(ch) - LowerOffset, nil
	} else {
		return 0, errors.New("invalid char")
	}
}

func true_chr(num int) (string, error) {
	if num < 10 {
		return string(rune(num + DigitOffset)), nil
	} else if num >= 10 && num <= 35 {
		return string(rune(num + UpperOffset)), nil
	} else if num >= 36 && num < 62 {
		return string(rune(num + LowerOffset)), nil
	}
	return "0", errors.New("Invalid integer")
}

func reverse(s string) string {
	r := []rune(s)
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	return string(r)
}
