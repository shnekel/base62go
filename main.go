package main

import (
	"fmt"
	"gitlab.com/shnekel/base62go/base62"
)

func main() {
	fmt.Println("saturate input", base62.Saturate("googlecom"))
	fmt.Println("dehydrate input", base62.Dehydrate(9349244358404308))
}
